using System;
using TravelRepublic.FlightCodingTest.Services;
using Xunit;

namespace TravelRepublic.FlightCodingTest.Search.Tests
{
    public class SpecificationTests
    {
        [Fact]
        public void ArrivesBeforeDepartsSpecification_ReturnsTrue_OnSatisfyingFlight()
        {
            var segments = new[] {new Segment {DepartureDate = DateTime.Now, ArrivalDate = DateTime.Now.AddDays(-1)}};
            var flight = new Flight {Segments = segments};
            var spec = Spec.ArrivesBeforeDeparts();
            
            Assert.True(spec.IsSatisfiedBy(flight));
        }
        
        [Fact]
        public void ArrivesBeforeDepartsSpecification_ReturnsFalse_OnUnsatisfyingFlight()
        {
            var segments = new[] {new Segment {DepartureDate = DateTime.Now, ArrivalDate = DateTime.Now.AddDays(2)}};
            var flight = new Flight {Segments = segments};
            var spec = Spec.ArrivesBeforeDeparts();
            
            Assert.False(spec.IsSatisfiedBy(flight));
        }

        [Fact]
        public void MinimalGroundTimeSpecification_ReturnsTrue_OnSatisfyingFlights()
        {
            var (dep1, arr1) = (DateTime.Now, DateTime.Now.AddHours(7));
            var (dep2, arr2) = (arr1.AddHours(3), dep1.AddDays(2));
            var segments = new[]
            {
                new Segment {DepartureDate = dep1, ArrivalDate = arr1},
                new Segment {DepartureDate = dep2, ArrivalDate = arr2}
            };
            var flight = new Flight {Segments = segments};
            var spec = Spec.MinGroundTime(TimeSpan.FromHours(2));
            
            Assert.True(spec.IsSatisfiedBy(flight));
        }
    }
}