using System.Collections.Generic;
using System.Threading.Tasks;

namespace TravelRepublic.FlightCodingTest.FlightProvider
{
    public interface IFlightProvider
    {
        Task<IList<Flight>> GetFlights();
    }
}