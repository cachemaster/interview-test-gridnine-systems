using System.Collections.Generic;
using System.Threading.Tasks;
using TravelRepublic.FlightCodingTest.Search;

namespace TravelRepublic.FlightCodingTest.FlightProvider
{
    public class DefaultFlightProvider : IFlightProvider
    {
        private static readonly FlightBuilder Builder = new FlightBuilder();

        public Task<IList<Flight>> GetFlights() => Task.FromResult(Builder.GetFlights());
    }
}