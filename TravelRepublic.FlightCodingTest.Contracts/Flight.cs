using System.Collections.Generic;

namespace TravelRepublic.FlightCodingTest
{
    public class Flight
    {
        public IList<Segment> Segments { get; set; }
    }
}