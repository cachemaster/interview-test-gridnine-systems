namespace TravelRepublic.FlightCodingTest.Search
{
    public interface IFlightSpecification
    {
        bool IsSatisfiedBy(Flight flight);
    }
}