using System.Collections.Generic;
using System.Threading.Tasks;

namespace TravelRepublic.FlightCodingTest.Search
{
    public interface IFlightSearchService
    {
        // We use only specifications.
        // I think it is a good idea to use specifications instead of filter object because it is more agile way
        // The specifications set must be resolved in user interaction layer, for example ASP.NET MVC controller action
        Task<IReadOnlyList<Flight>> SearchAsync(params IFlightSpecification[] specifications);
    }
}