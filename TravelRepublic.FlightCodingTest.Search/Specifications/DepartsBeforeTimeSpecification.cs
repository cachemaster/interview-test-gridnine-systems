using System;
using System.Linq;

namespace TravelRepublic.FlightCodingTest.Search.Specifications
{
    public class DepartsBeforeTimeSpecification : IFlightSpecification
    {
        private readonly DateTime _time;

        public DepartsBeforeTimeSpecification(DateTime time) => _time = time;

        public bool IsSatisfiedBy(Flight flight) => 
            flight.Segments != null && flight.Segments.Count != 0 && flight.Segments.First().DepartureDate < _time;
    }
}