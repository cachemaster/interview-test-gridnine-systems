using System;

namespace TravelRepublic.FlightCodingTest.Search.Specifications
{
    public class MinimalGroundTimeSpecification : IFlightSpecification
    {
        private readonly TimeSpan _time;


        public MinimalGroundTimeSpecification(TimeSpan time) => _time = time;
        
        public bool IsSatisfiedBy(Flight flight)
        {
            if (flight.Segments == null || flight.Segments.Count < 2)
                return false;

            for (var i = 0; i < flight.Segments.Count - 1; ++i)
            {
                if (flight.Segments[i + 1].DepartureDate - flight.Segments[i].ArrivalDate > _time)
                    return true;
            }

            return false;
        }
    }
}