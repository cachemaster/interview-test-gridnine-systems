using System;
using TravelRepublic.FlightCodingTest.Search.Specifications;

namespace TravelRepublic.FlightCodingTest.Services
{
    public static class Spec
    {
        public static ArrivesBeforeDepartsSpecification ArrivesBeforeDeparts() => 
            ArrivesBeforeDepartsSpecification.Shared;
        
        public static DepartsBeforeTimeSpecification DepartsBefore(DateTime time) =>
            new DepartsBeforeTimeSpecification(time);
        
        public static MinimalGroundTimeSpecification MinGroundTime(TimeSpan time) =>
            new MinimalGroundTimeSpecification(time);
    }
}