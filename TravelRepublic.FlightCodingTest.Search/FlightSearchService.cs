using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TravelRepublic.FlightCodingTest.FlightProvider;


namespace TravelRepublic.FlightCodingTest.Search
{
    public class FlightSearchService : IFlightSearchService
    {
        private readonly IFlightProvider _provider;


        public FlightSearchService(IFlightProvider provider) =>
            _provider = provider ?? throw new ArgumentNullException(nameof(provider)); 
        
        public async Task<IReadOnlyList<Flight>> SearchAsync(params IFlightSpecification[] specifications)
        {
            var flights = await _provider.GetFlights();
            
            return flights
                .Where(f => specifications.All(s => s.IsSatisfiedBy(f)))
                .ToArray();
        }
    }
}