﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TravelRepublic.FlightCodingTest.FlightProvider;
using TravelRepublic.FlightCodingTest.Search;
using TravelRepublic.FlightCodingTest.Services;

namespace TravelRepublic.FlightCodingTest.App
{
    internal static class Program
    {
        // In the real application we will use the DI container to init the dependencies
        private static readonly IFlightSearchService Service = new FlightSearchService(new DefaultFlightProvider());


        private static async Task Main(string[] args)
        {
            Console.WriteLine("Flights departed in the past:");
            PrintFlights(await SearchForFlightDepartedInThePast());
            
            Console.WriteLine("Flights with arrival time before the departure time:");
            PrintFlights(await SearchForFlightsWithArriveBeforeDeparture());
            
            Console.WriteLine("Flights with more than two hours on the ground:");
            PrintFlights(await SearchForFlightsWithMoreThanTwoHoursOnTheGround());
        }

        private static void PrintFlights(IEnumerable<Flight> flights)
        {
            var flightOrder = 1;
            
            foreach (var flight in flights)
            {
                Console.WriteLine($"Flight {flightOrder++}");
                
                foreach (var segment in flight.Segments)
                {
                    Console.WriteLine($"Departure: {segment.DepartureDate}, Arrive: {segment.ArrivalDate}");
                }
            }
        }
        
        private static Task<IReadOnlyList<Flight>> SearchForFlightDepartedInThePast() => 
            Service.SearchAsync(Spec.DepartsBefore(DateTime.Now));
        
        private static Task<IReadOnlyList<Flight>> SearchForFlightsWithArriveBeforeDeparture() =>
            Service.SearchAsync(Spec.ArrivesBeforeDeparts());
        
        private static Task<IReadOnlyList<Flight>> SearchForFlightsWithMoreThanTwoHoursOnTheGround() =>
            Service.SearchAsync(Spec.MinGroundTime(TimeSpan.FromHours(2)));
    }
}